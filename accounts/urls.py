from django.urls import path
from accounts.views import user_login, signup


urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_login, name="logout"),
    path("signup/", signup, name="signup"),
]
